import propertiesProviderModule from 'bpmn-js-properties-panel/lib/provider/camunda';
const [type, propertiesProvider] = propertiesProviderModule.propertiesProvider;

function CamundaPropertiesProvider(...args) {
    const provider = new propertiesProvider(...args);
    const getTabs = provider.getTabs.bind(provider);
    provider.getTabs = function(...args2) {
        const tabs = getTabs(...args2);
        const tabs1 = tabs.map(tab => {
            if (tab.id !== 'general') {
                tab.enabled = () => false;
            } else {
                // Fix the typo from '.foreach' to '.forEach'
                tab.groups.forEach((group) => {
                    if (group.id === 'general') {
                        group.entries = group.entries.filter((item) => ['id', 'name'].includes(item.id));
                        return;
                    }
                    if (group.id === 'details') {
                        group.entries = group.entries.filter((item) => ['candidateGroups', 'condition'].includes(item.id));
                        return;
                    }
                    group.enabled = () => false;
                });
            }
            return tab;
        });
        return tabs1;
    };
    return provider;
}

CamundaPropertiesProvider.$inject = propertiesProvider.$inject;

export default {
    ...propertiesProviderModule,
    propertiesProvider: [type, CamundaPropertiesProvider]
};