/*
 * @Author: srd15850773394 tangdongyao@xinchan.com.cn
 * @Date: 2024-08-28 11:55:23
 * @LastEditors: srd15850773394 tangdongyao@xinchan.com.cn
 * @LastEditTime: 2024-08-28 15:04:53
 * @FilePath: /my-activiti-vue2/src/main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import "bpmn-js-properties-panel/dist/assets/bpmn-js-properties-panel.css"
import"bpmn-js/dist/assets/diagram-js.css";
import"bpmn-js/dist/assets/bpmn-font/css/bpmn.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css"
new Vue({
  render: h => h(App),
}).$mount('#app')
